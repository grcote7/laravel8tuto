<?php

use Illuminate\Support\Facades\Route;
//Import page controller
use App\Http\Controllers\Video8;
//Import page controller
use App\Http\Controllers\Video8ViewController;
//Import page controller
use App\Http\Controllers\Video10Controller;
//import page Video12Controller from controller
use App\Http\Controllers\Video12Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::view('firstFile', 'firstFile');

// add Routing for about and contact pages
Route::view('about', 'about')->name('about');
Route::view('contact', 'contact')->name('contact');

// add routing for controller
// Route get('path', 'controller File')   Il faut utiliser get au lieu de view pour retourner une vue depuis le controller
Route::get('video8', [Video8::class, 'index']);

//vidéo 8
Route::get('video8V', [Video8ViewController::class, 'loadView']);

// Video 10
Route::get('video10', [Video10Controller::class, 'LoadView']);

// video 12
Route::post('video12', [Video12Controller::class, 'getData']);
Route::view('video12', 'video12');

//video 14
Route::view('video14Home', 'video14Home');
Route::view('video14Noaccess', 'video14Noaccess');
Route::view('video14Users', 'video14Users');

//Video 15
Route::view('video15Home', 'video15Home');
Route::view('video15Noaccess', 'video15Noaccess');
Route::view('video15Users', 'video15Users')->middleware('protectedPage');  //video 16 Route middleware 
//Route::view('video15Users', 'video15Users');
// la Route pous la vue
//Route::group(['middleware' => ['protectedPage']], function(){
//    Route::view('video15Users', 'video15Users');
//});

