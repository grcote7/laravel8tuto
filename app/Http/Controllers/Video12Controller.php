<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Video12Controller extends Controller
{
    //
    function getData(Request $req) // Il est obligatoire de renseigner un parametre et mettre Request comme ==> Request $req
    {
        return $req->validate([
            'username' => 'required | max:10',
            'userpassword' => 'required  | min:5',
        ]);

        return $req->input();
    }
}   
