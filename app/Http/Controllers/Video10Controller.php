<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Video10Controller extends Controller
{
    //
    public function LoadView()
    {
        // exemple 1
        //return view('video10', ['users'=>['Sam', 'Mo', 'Dan']]);
        // exemple 2
        //return view('video10', ['user'=>'Mo']);
        //exemple 3
        $data = ['Mo', 'Lio', 'Ablaye', 'Malick', 'Aziz', 'Abou', 'Banel', 'Yaye', 'Adou'];
        return view('video10', ['users' =>$data]);
    }
}
